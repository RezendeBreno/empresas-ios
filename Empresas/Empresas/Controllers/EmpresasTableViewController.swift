//
//  EmpresasTableViewController.swift
//  Empresas
//
//  Created by Breno Rezende on 07/12/19.
//  Copyright © 2019 Breno Rezende. All rights reserved.
//

import UIKit

class EmpresasTableViewController: UITableViewController {

    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
        self.setupNavBar()
        self.setupSearchController()
    }
    
    func setupView() {
        self.tableView.tableFooterView = UIView()
        
        let beginLabel = UILabel()
        beginLabel.translatesAutoresizingMaskIntoConstraints = false
        beginLabel.textColor = .charcoalGrey
        beginLabel.text = "Clique na busca para iniciar."
        self.view.addSubview(beginLabel)
        
        beginLabel.centerYAnchor.constraint(equalTo: self.tableView.centerYAnchor).isActive = true
        beginLabel.centerXAnchor.constraint(equalTo: self.tableView.centerXAnchor).isActive = true
    }
    
    func setupNavBar() {
        
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logoNavBar"))
        
        searchBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showSearchBar))
        searchBarButtonItem?.tintColor = .white
        self.navigationItem.rightBarButtonItem = searchBarButtonItem
    }
    
    func setupSearchController() {
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Pesquisar"
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.tintColor = .darkishPink
        
        // Change searchBar's UITextField color
        let searchField = self.searchBar.value(forKey: "searchField") as? UITextField
        if let searchField = searchField {
        
            searchField.backgroundColor = .white
            searchField.textColor = .darkIndigo
        }
        
        // Change cancel button color
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
    }
    
    @objc func showSearchBar() {
        self.searchBar.alpha = 0.0
        
        UIView.animate(withDuration: 0.2) {
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.titleView = self.searchBar
            self.searchBar.alpha = 1.0
            self.searchBar.becomeFirstResponder()
        }
    }
}

extension EmpresasTableViewController: UISearchControllerDelegate, UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logoNavBar"))
        self.navigationItem.rightBarButtonItem = self.searchBarButtonItem
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Texto buscado: \(searchText)")
    }
}
