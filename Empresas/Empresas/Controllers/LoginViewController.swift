//
//  LoginViewController.swift
//  Empresas
//
//  Created by Breno Rezende on 07/12/19.
//  Copyright © 2019 Breno Rezende. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    private var userViewModel = UserViewModel()
    
    @IBOutlet weak var emailTextField: EmpresasTextField!
    @IBOutlet weak var passwordTextField: EmpresasTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func entrarTouched(_ sender: UIButton) {
        self.login()
    }
}

// MARK : - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            if textField == emailTextField {
                userViewModel.updateEmail(email: text)
            } else if textField == passwordTextField {
                userViewModel.updatePassword(password: text)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailTextField:
            passwordTextField.becomeFirstResponder()
        case self.passwordTextField:
            self.passwordTextField.resignFirstResponder()
            self.login()
        default:
            self.view.endEditing(true)
        }
        
        return true
    }
}

// MARK: - Class Methods
extension LoginViewController {
    
    fileprivate func showErrorMessage(_ error: (String)) {
        let alertController = UIAlertController(title: "Falha na autenticação", message: error, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func login() {
        
        switch userViewModel.validate() {
            case .Valid:
                userViewModel.login { (errorString) in
                    if let error = errorString {
                        self.showErrorMessage(error)
                        return
                    }
                    
                    let newViewController = self.storyboard?.instantiateViewController(identifier: "inicialNavController") as? UINavigationController
                    self.view.window?.rootViewController = newViewController
                }
            case .Invalid(let error):
                showErrorMessage(error)
        }
    }
}
