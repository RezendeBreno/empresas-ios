//
//  UserService.swift
//  Empresas
//
//  Created by Breno Rezende on 07/12/19.
//  Copyright © 2019 Breno Rezende. All rights reserved.
//

import Foundation

struct UserService {
    
    static let shared = UserService()
    
    private init() {}
    
    func loginWithEmail(_ email: String, password: String, completion: @escaping (String?) -> ()) {
        

        let params: [String : String] = [
            "email" : email,
            "password" : password
        ]
        let endpoing = "users/auth/sign_in"

        HttpService.shared.request(method: .post, endpoint: endpoing, params: params) { (data, errorMessage) in
            if let error = errorMessage {
                return completion(error)
            }
            
            //TODO: Decode the data, however the API isn't working atm.
        }
    }
    
}
