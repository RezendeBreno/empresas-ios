//
//  HttpService.swift
//  Empresas
//
//  Created by Breno Rezende on 08/12/19.
//  Copyright © 2019 Breno Rezende. All rights reserved.
//

import Foundation
import Alamofire

struct API {
    static let host = "http://empresas.ioasys.com.br"
    static let apiVersion = "v1"
    static var accessToken: String?
    static var client: String?
    static var uid: String?
}

struct HttpService {
    
    static let shared = HttpService()
    
    private init() {}
    
    func request(method: HTTPMethod, endpoint: String, params: [String : String], completion: @escaping (Data?, String?) -> ()) {
        
        var headers: HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        if let accessToken = API.accessToken, let client = API.client, let uid = API.uid {
            headers["access-token"] = accessToken
            headers["client"] = client
            headers["uid"] = uid
        }
        
        guard let url = URL(string: "\(API.host)/api/\(API.apiVersion)/\(endpoint)") else { return }
        
        Alamofire.request(url, method: method, parameters: params, encoding: URLEncoding.default, headers: headers).validate(statusCode: 200..<300).responseData { (response) in
            
            if response.result.isSuccess {
                return completion(response.result.value, nil)
            } else {
                return completion(nil, response.result.error?.localizedDescription)
            }
        }
    }
}
