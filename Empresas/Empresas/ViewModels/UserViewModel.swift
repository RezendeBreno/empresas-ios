//
//  UserViewModel.swift
//  Empresas
//
//  Created by Breno Rezende on 07/12/19.
//  Copyright © 2019 Breno Rezende. All rights reserved.
//

import Foundation

enum UserValidateState {
    case Valid
    case Invalid(String)
}

struct UserViewModel {
    
    private var user = User()
    
    var email: String {
        return self.user.email
    }
    
    var password: String {
        return self.user.password
    }
}

extension UserViewModel {
    
    mutating func updateEmail(email: String) {
        self.user.email = email
    }
    
    mutating func updatePassword(password: String) {
        self.user.password = password
    }
    
    func validate() -> UserValidateState {
                
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailPred.evaluate(with: self.user.email) {
            return .Invalid("E-mail inválido")
        }
        
        if self.user.password.isEmpty {
            return .Invalid("Senha inválida")
        }
        
        return .Valid
    }
    
    func login(completion: @escaping (_ errorString: String?) -> ()) {
        UserService.shared.loginWithEmail(user.email, password: user.password) { errorString in
            
            if let error = errorString {
                return completion(error)
            }
            
            return completion(nil)
        }
    }
}
