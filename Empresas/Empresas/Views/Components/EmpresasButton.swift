//
//  EmpresasButton.swift
//  Empresas
//
//  Created by Breno Rezende on 07/12/19.
//  Copyright © 2019 Breno Rezende. All rights reserved.
//

import UIKit

class EmpresasButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    func setup() {
        
        self.backgroundColor = UIColor.greenyBlue
        self.layer.cornerRadius = 8
        
        // title settings
        self.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        self.setTitleColor(UIColor.white, for: .normal)
    }
}
