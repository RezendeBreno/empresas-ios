//
//  EmpresasTextField.swift
//  Empresas
//
//  Created by Breno Rezende on 07/12/19.
//  Copyright © 2019 Breno Rezende. All rights reserved.
//

import UIKit

class EmpresasTextField: UITextField {

    private let bottomLine = CALayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        bottomLine.frame = CGRect(x: 0.0, y: self.frame.size.height, width: self.frame.size.width, height: 1.0)
    }
    
    func setup() {
        
        // Add bottom line
        bottomLine.backgroundColor = UIColor.charcoalGrey.cgColor
        
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)

        // font settings
        self.font = UIFont.systemFont(ofSize: 18)
    }
    
}

extension EmpresasTextField {
    
    @IBInspectable
    public var leftImage: UIImage? {
        get {
            return self.leftImage
        }
        set {
            self.leftViewMode = .always
            self.leftView = UIImageView(image: newValue)
        }
    }
    
}
