//
//  UIColor+Additions.swift
//  Teste_iOS
//
//  Generated on Zeplin. (12/7/2019).
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIColor {

    @nonobjc class var black: UIColor {
    return UIColor(white: 3.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var mediumPink: UIColor {
    return UIColor(red: 238.0 / 255.0, green: 76.0 / 255.0, blue: 119.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var darkIndigo: UIColor {
    return UIColor(red: 26.0 / 255.0, green: 14.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var steel: UIColor {
    return UIColor(red: 142.0 / 255.0, green: 142.0 / 255.0, blue: 147.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var coolGrey60: UIColor {
    return UIColor(red: 171.0 / 255.0, green: 179.0 / 255.0, blue: 189.0 / 255.0, alpha: 0.6)
    }

    @nonobjc class var coolGrey30: UIColor {
    return UIColor(red: 171.0 / 255.0, green: 179.0 / 255.0, blue: 189.0 / 255.0, alpha: 0.3)
    }

    @nonobjc class var coolGrey50: UIColor {
    return UIColor(red: 171.0 / 255.0, green: 179.0 / 255.0, blue: 189.0 / 255.0, alpha: 0.5)
    }
    
    @nonobjc class var charcoalGrey: UIColor {
      return UIColor(red: 56.0 / 255.0, green: 55.0 / 255.0, blue: 67.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var black50: UIColor {
    return UIColor(white: 0.0, alpha: 0.5)
    }

    @nonobjc class var white10: UIColor {
    return UIColor(white: 1.0, alpha: 0.1)
    }

    @nonobjc class var softGreen: UIColor {
    return UIColor(red: 125.0 / 255.0, green: 192.0 / 255.0, blue: 117.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var warmGrey: UIColor {
    return UIColor(red: 141.0 / 255.0, green: 140.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var greenyBlue: UIColor {
        return UIColor(named: "greenyBlue")!
    }

    @nonobjc class var darkishPink: UIColor {
    return UIColor(red: 222.0 / 255.0, green: 71.0 / 255.0, blue: 114.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var beige: UIColor {
        return UIColor(named: "beige")!
    }

}
